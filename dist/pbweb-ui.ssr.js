'use strict';function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];

  if (_i == null) return;
  var _arr = [];
  var _n = true;
  var _d = false;

  var _s, _e;

  try {
    for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}var script$3 = {
  name: 'PbwebUiSample',
  // vue component name
  data: function data() {
    return {
      counter: 5,
      initCounter: 5,
      message: {
        action: null,
        amount: null
      }
    };
  },
  computed: {
    changedBy: function changedBy() {
      var message = this.message;
      if (!message.action) return 'initialized';
      return "".concat(message.action, " ").concat(message.amount || '').trim();
    }
  },
  methods: {
    increment: function increment(arg) {
      var amount = typeof arg !== 'number' ? 1 : arg;
      this.counter += amount;
      this.message.action = 'incremented by';
      this.message.amount = amount;
    },
    decrement: function decrement(arg) {
      var amount = typeof arg !== 'number' ? 1 : arg;
      this.counter -= amount;
      this.message.action = 'decremented by';
      this.message.amount = amount;
    },
    reset: function reset() {
      this.counter = this.initCounter;
      this.message.action = 'reset';
      this.message.amount = null;
    }
  }
};function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}function createInjectorSSR(context) {
    if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
    }
    if (!context)
        return () => { };
    if (!('styles' in context)) {
        context._styles = context._styles || {};
        Object.defineProperty(context, 'styles', {
            enumerable: true,
            get: () => context._renderStyles(context._styles)
        });
        context._renderStyles = context._renderStyles || renderStyles;
    }
    return (id, style) => addStyle(id, style, context);
}
function addStyle(id, css, context) {
    const group = css.media || 'default' ;
    const style = context._styles[group] || (context._styles[group] = { ids: [], css: '' });
    if (!style.ids.includes(id)) {
        style.media = css.media;
        style.ids.push(id);
        let code = css.source;
        style.css += code + '\n';
    }
}
function renderStyles(styles) {
    let css = '';
    for (const key in styles) {
        const style = styles[key];
        css +=
            '<style data-vue-ssr-id="' +
                Array.from(style.ids).join(' ') +
                '"' +
                (style.media ? ' media="' + style.media + '"' : '') +
                '>' +
                style.css +
                '</style>';
    }
    return css;
}/* script */
var __vue_script__$3 = script$3;
/* template */

var __vue_render__$3 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticClass: "pbweb-ui-sample"
  }, [_vm._ssrNode("<p data-v-9bf90622>" + _vm._ssrEscape("The counter was " + _vm._s(_vm.changedBy) + " to ") + "<b data-v-9bf90622>" + _vm._ssrEscape(_vm._s(_vm.counter)) + "</b>.</p> <button data-v-9bf90622>\n    Click +1\n  </button> <button data-v-9bf90622>\n    Click -1\n  </button> <button data-v-9bf90622>\n    Click +5\n  </button> <button data-v-9bf90622>\n    Click -5\n  </button> <button data-v-9bf90622>\n    Reset\n  </button>")]);
};

var __vue_staticRenderFns__$3 = [];
/* style */

var __vue_inject_styles__$3 = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-9bf90622_0", {
    source: ".pbweb-ui-sample[data-v-9bf90622]{display:block;width:400px;margin:25px auto;border:1px solid #ccc;background:#eaeaea;text-align:center;padding:25px}.pbweb-ui-sample p[data-v-9bf90622]{margin:0 0 1em}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__$3 = "data-v-9bf90622";
/* module identifier */

var __vue_module_identifier__$3 = "data-v-9bf90622";
/* functional template */

var __vue_is_functional_template__$3 = false;
/* style inject shadow dom */

var __vue_component__$6 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$3,
  staticRenderFns: __vue_staticRenderFns__$3
}, __vue_inject_styles__$3, __vue_script__$3, __vue_scope_id__$3, __vue_is_functional_template__$3, __vue_module_identifier__$3, false, undefined, createInjectorSSR, undefined);

var __vue_component__$7 = __vue_component__$6;//
//
//
//
//
//
//
//
//
var script$2 = {
  name: "MyPBTItem",
  props: ["info", "active"],
  computed: {
    cls: function cls() {
      if (this.active) return "my-pbt-item active";
      return "my-pbt-item";
    }
  }
};/* script */
var __vue_script__$2 = script$2;
/* template */

var __vue_render__$2 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('el-col', {
    directives: [{
      name: "loading",
      rawName: "v-loading",
      value: _vm.info.loading,
      expression: "info.loading"
    }],
    class: _vm.cls
  }, [_c('div', {
    staticClass: "idbox"
  }, [_c('p', [_vm._v("#" + _vm._s(_vm.info.id))])]), _vm._v(" "), 'meta' in _vm.info ? _c('img', {
    directives: [{
      name: "lazy",
      rawName: "v-lazy",
      value: _vm.info.meta.image,
      expression: "info.meta.image"
    }],
    attrs: {
      "alt": "img"
    }
  }) : _vm._e()]);
};

var __vue_staticRenderFns__$2 = [];
/* style */

var __vue_inject_styles__$2 = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-68984e84_0", {
    source: ".my-pbt-item[data-v-68984e84]{display:block;position:relative;width:200px;height:160px;text-align:center}.idbox[data-v-68984e84]{position:absolute;top:0;left:0}p[data-v-68984e84]{font-family:Oswald;font-size:16px;color:#fff;font-weight:500;line-height:24px}img[data-v-68984e84]{margin:20px 0 0 20px;border-radius:8px;width:128px;height:128px}.active[data-v-68984e84]{border:1px solid #38f2af}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__$2 = "data-v-68984e84";
/* module identifier */

var __vue_module_identifier__$2 = "data-v-68984e84";
/* functional template */

var __vue_is_functional_template__$2 = false;
/* style inject shadow dom */

var __vue_component__$4 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$2,
  staticRenderFns: __vue_staticRenderFns__$2
}, __vue_inject_styles__$2, __vue_script__$2, __vue_scope_id__$2, __vue_is_functional_template__$2, __vue_module_identifier__$2, false, undefined, createInjectorSSR, undefined);

var __vue_component__$5 = __vue_component__$4;//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var script$1 = {
  name: "SellingItem",
  props: ["info"]
};/* script */
var __vue_script__$1 = script$1;
/* template */

var __vue_render__$1 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('el-col', {
    directives: [{
      name: "loading",
      rawName: "v-loading",
      value: _vm.info.loading,
      expression: "info.loading"
    }],
    staticClass: "selling-item"
  }, ['meta' in _vm.info ? _c('img', {
    directives: [{
      name: "lazy",
      rawName: "v-lazy",
      value: _vm.info.meta.image,
      expression: "info.meta.image"
    }],
    attrs: {
      "alt": "img"
    }
  }) : _vm._e(), _vm._v(" "), _c('el-col', {
    staticClass: "item-info"
  }, [_c('el-col', {
    staticClass: "item-desc"
  }, [_c('span', [_vm._v("#" + _vm._s(_vm.info.id))]), _vm._v(" "), _c('br'), _vm._v(" "), _vm.info.market ? _c('span', {
    staticClass: "desc"
  }, [_vm._v(_vm._s(_vm.info.market.desc))]) : _vm._e()]), _vm._v(" "), _vm.info.market ? _c('el-col', {
    staticClass: "item-owner"
  }, [_vm.info.market.seller == '-self' ? _c('el-col', {
    staticClass: "sell-info"
  }, [_vm._v("My Sale")]) : _vm._e(), _vm._v(" "), _c('el-col', {
    staticClass: "item-price"
  }, [_vm._v(_vm._s(_vm.info.market.price) + " " + _vm._s(_vm.info.market.ptName))])], 1) : _vm._e()], 1)], 1);
};

var __vue_staticRenderFns__$1 = [];
/* style */

var __vue_inject_styles__$1 = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-3cba5806_0", {
    source: "*[data-v-3cba5806]{color:#38f2af}.selling-item[data-v-3cba5806]{display:block;width:258px;height:400px;margin:18px 12.5px;background:#383940;box-sizing:border-box;border-radius:8px;position:relative;overflow:hidden}.selling-item img[data-v-3cba5806]{display:inline-block;width:258px;height:258px;border-radius:8px 8px 0 0;position:absolute;top:-5%}.item-info[data-v-3cba5806]{background:#272a34;padding:18px;width:258px;position:absolute;top:55%}.item-desc[data-v-3cba5806]{height:100px;border-bottom:2px solid rgba(255,255,255,.08)}.desc[data-v-3cba5806]{color:#828282}.item-owner[data-v-3cba5806]{height:56px;padding-top:20px;position:relative}.sell-info[data-v-3cba5806]{position:absolute}.item-owner .el-co[data-v-3cba5806]:first-child{float:left}.item-price[data-v-3cba5806]{float:right;font-size:14px;font-family:Oswald;line-height:21px;text-align:center;height:21px;width:75px;border-radius:10px;border:#38f2af 1px solid}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__$1 = "data-v-3cba5806";
/* module identifier */

var __vue_module_identifier__$1 = "data-v-3cba5806";
/* functional template */

var __vue_is_functional_template__$1 = false;
/* style inject shadow dom */

var __vue_component__$2 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$1,
  staticRenderFns: __vue_staticRenderFns__$1
}, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, false, undefined, createInjectorSSR, undefined);

var __vue_component__$3 = __vue_component__$2;//
//
//
//
//
//
//
//
//
//
//
//
//
var script = {
  name: "MylistPage",
  props: ["nftlist", "open", "current"],
  data: function data() {
    return {};
  }
};/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('el-col', {
    attrs: {
      "id": "listpage"
    }
  }, [_c('ul', _vm._l(_vm.nftlist, function (nft) {
    return _c('li', {
      key: nft.id,
      staticClass: "nftli",
      attrs: {
        "info": nft
      }
    }, [_c('MyPBTItem', {
      attrs: {
        "info": nft,
        "active": _vm.current.pbtId == nft.id
      },
      nativeOn: {
        "click": function click($event) {
          return _vm.open(nft.id);
        }
      }
    })], 1);
  }), 0)]);
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-79beb3af_0", {
    source: "#listpage[data-v-79beb3af]{width:250px;height:650px;background:#25272e;box-sizing:border-box}#listpage ul li[data-v-79beb3af]{height:200px;width:250px;padding:10px;border-bottom:2px solid rgba(255,255,255,.08);background-color:#25272e}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = "data-v-79beb3af";
/* module identifier */

var __vue_module_identifier__ = "data-v-79beb3af";
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, undefined, createInjectorSSR, undefined);

var __vue_component__$1 = __vue_component__;/* eslint-disable import/prefer-default-export */var components$1=/*#__PURE__*/Object.freeze({__proto__:null,PbwebUiSample:__vue_component__$7,MyPBTItem:__vue_component__$5,SellingItem:__vue_component__$3,MylistPage:__vue_component__$1});var install = function installPbwebUi(Vue) {
  Object.entries(components$1).forEach(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        componentName = _ref2[0],
        component = _ref2[1];

    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()
var components=/*#__PURE__*/Object.freeze({__proto__:null,'default':install,PbwebUiSample:__vue_component__$7,MyPBTItem:__vue_component__$5,SellingItem:__vue_component__$3,MylistPage:__vue_component__$1});// only expose one global var, with component exports exposed as properties of
// that global var (eg. plugin.component)

Object.entries(components).forEach(function (_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      componentName = _ref2[0],
      component = _ref2[1];

  if (componentName !== 'default') {
    install[componentName] = component;
  }
});module.exports=install;